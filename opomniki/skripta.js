window.addEventListener('load', function() {
	//stran nalozena

	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");

		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);

			//TODO:
			// - če je čas enak 0, izpiši opozorilo "Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!"
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
			if (cas == 0) {
				var nazivO = opomnik.querySelector('.naziv_opomnika').innerHTML;
				
				alert("Opomnik!\n\nZadolžitev "+nazivO+" je potekla!");
				opomnik.parentElement.removeChild(opomnik);
			} else {
				cas--;
				opomnik.querySelector("span").innerHTML = cas;
			}
		}
	}
	setInterval(posodobiOpomnike, 1000);

	var koKlik = function() {
		var ime = document.querySelector('#uporabnisko_ime').value;
		document.querySelector('#uporabnik').innerHTML = ime;
		document.querySelector('.pokrivalo').style.display='none';
	}
	var gumbPotrdi = document.querySelector("input[type='button']");
	gumbPotrdi.addEventListener('click', koKlik);
	
	var dodajOpomnik = function() {
		var naziv = document.querySelector('#naziv_opomnika').value;
		var cas = document.querySelector('#cas_opomnika').value;
		
		document.querySelector('#naziv_opomnika').value = "";
		document.querySelector('#cas_opomnika').value = "";
		
		var nodeBig = document.createElement('div');
		nodeBig.setAttribute('class', 'opomnik senca rob');
		
		
		var node = document.createElement('div');
		node.setAttribute('class', 'naziv_opomnika');
		node.appendChild(document.createTextNode(naziv));
		nodeBig.appendChild(node);
			
		node = document.createElement('div');
		node.setAttribute('class', 'cas_opomnika');
		node.appendChild(document.createTextNode(' Opomnik čez '));
		var nodeSpan = document.createElement('span');
		nodeSpan.appendChild(document.createTextNode(cas));
		node.appendChild(nodeSpan);
		node.appendChild(document.createTextNode(' sekund.'));
		nodeBig.appendChild(node);	
			
		document.querySelector('#opomniki').appendChild(nodeBig);
		//document.querySelector('.naziv_opomnika').innerHTML = naziv;
		//document.querySelector('.cas_opomnika').querySelector('span').innerHTML = cas;
	}
	document.getElementById('dodajGumb').addEventListener('click', dodajOpomnik);
});
